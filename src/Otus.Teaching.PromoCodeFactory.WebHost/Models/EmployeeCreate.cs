﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models;

public class EmployeeCreate
{
    public string FirstName { get; set; }
    
    public string LastName { get; set; }
}