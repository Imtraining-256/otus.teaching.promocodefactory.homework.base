﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var isDeleted = Data.Any(e => e.Id == id);
            if (isDeleted)
            {
                await Task.Run(() =>
                {
                    Data = Data.Where(e => e.Id != id);
                    return true;
                });
            }
            return isDeleted;
        }
        
        public async Task<Guid> CreateAsync(T emp)
        {
            var id = Guid.NewGuid();
            await Task.Run(() => 
            {
                emp.Id = id;
                Data = Data.Append(emp);
            });
            return id;
        }
        
        public async Task<T> UpdateAsync(T emp)
        {
            var isUpd = Data.Any(e => e.Id == emp.Id);
            if (isUpd)
            {
                await Task.Run(() =>
                {
                    DeleteAsync(emp.Id);
                    CreateAsync(emp);
                });
            }

            return emp;
        }

    }
}